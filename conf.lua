function love.conf(t)
	t.width = 128
	t.height = 128

	t.console = true
	t.version = "11.2"
end
