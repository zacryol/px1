-- Window size (not game size. Game size is 1)
local x = 128
local y = 128

-- vars for run data
local current_score = 0
local high_score = 0 -- Only for current play session unless I add saves

local time = 0
local round_time = 5
local round_time_init = 5

-- sounds
local ded = love.audio.newSource("sfx/Ded.wav", "static")
ded:setVolume(0.85)
local start = love.audio.newSource("sfx/Start.wav", "static")
start:setVolume(0.85)
local good = love.audio.newSource("sfx/Good.wav", "static")
good:setVolume(0.85)

-- Data for game states
-- c is Color shown; j is joystick input required
local state = 0
local stateCount = 8
local action_data = { 
	-- Black (no running game)
	[0] = {["c"] = {0, 0, 0}, ["j"] = "start", ["k"] = "return"},
	-- Green
	[1] = {["c"] = {0, 1, 0}, ["j"] = "a", ["k"] = "z"},
	-- Red
	[2] = {["c"] = {1, 0, 0}, ["j"] = "b", ["k"] = "x"},
	-- Blue
	[3] = {["c"] = {0, 0, 1}, ["j"] = "x", ["k"] = "c"},
	-- Yellow
	[4] = {["c"] = {1, 1, 0}, ["j"] = "y", ["k"] = "v"},
	-- Orange
	[5] = {["c"] = {1, 0.25, 0}, ["j"] = "dpup", ["k"] = "up"},
	-- Purple
	[6] = {["c"] = {0.5, 0, 0.5}, ["j"] = "dpdown", ["k"] = "down"},
	-- Cyan
	[7] = {["c"] = {0, 1, 1}, ["j"] = "dpleft", ["k"] = "left"},
	-- Pink
	[8] = {["c"] = {1, 0.5, 0.5}, ["j"] = "dpright", ["k"] = "right"},
}

function love.load()
	love.window.setTitle("PX1")
	love.window.setMode(x, y, {})
end


function love.gamepadpressed(joystick, button)
	checkInput(button, "j")
end


function love.keypressed(key, scancode, isrepeat)
	print(scancode)
	checkInput(scancode, "k")
end


function love.update(dt)
	if gameIsRunning()
	then
		time = time - dt
		if time < 0
		then
			endGame()
		end
	end
end


function love.draw()
	love.graphics.scale(x, y)
	love.graphics.setColor(action_data[state]["c"])
	love.graphics.rectangle("fill", 0, 0, 1, 1)
end


function checkInput(input, compareType)
	correct = input == action_data[state][compareType]
	if not gameIsRunning() and correct
	then
		startGame()
	elseif gameIsRunning() and not correct
	then
		endGame()
	elseif gameIsRunning() and correct
	then
		advanceRound()
	end
end


function gameIsRunning()
	return state ~= 0
end


function startGame()
	round_time = round_time_init
	current_score = 0
	state = love.math.random(stateCount)
	time = round_time
	start:play()
end


function advanceRound()
	state = love.math.random(stateCount)
	current_score = current_score + 1
	round_time = math.max(round_time * 0.99, 0.5)
	time = round_time
	good:play()
end


function endGame()
	state = 0
	high_score = math.max(high_score, current_score)
	print("Score this run: ", current_score)
	print("Best this session: ", high_score)
	f = io.open(love.filesystem.getSourceBaseDirectory() .. "/score.txt", "w")
	f:write("Score this run: " .. current_score .. "\r\n")
	f:write("Best this session: " .. high_score .. "\r\n")
	f:close()
	ded:play()
end
