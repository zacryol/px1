# PX1

The one-pixel game. (Made for LOWREZ Jam 2020 on itch.io)

## How to play:
This is a game of quick reflexes. The color of the pixel determines what button/key you must input. Be quick: you have limited time, and that time only gets stricter!

### Color Guide:

 * Black: The game isn't running. Press Start/Enter to begin!
 * Green: XBox A / Keyboard Z
 * Red: XBox B / Keyboard X
 * Blue: XBox X / Keyboard C
 * Yellow: XBox Y / Keyboard V
 * Orange: D-pad / Arrow Key Up
 * Purple: D-pad / Arrow Key Down
 * Cyan: D-pad / Arrow Key Left
 * Pink: D-pad / Arrow Key Right

 ## Installation:
 Requires installing [LÖVE](https://love2d.org/), version 11.2

 Packaged `.love` file also available on [itch](https://zacryol.itch.io/px1).
